import axios from "axios";
import React, { useEffect, useState } from "react";
import Navbar from "../components/Navbar";

function ProfilePage() {
  const [data, setData] = useState({});

  const fetchProfile = async () => {
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/profile",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setData(response.data.data);
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };

  useEffect(() => {
    fetchProfile();
  }, []);

  return (
    <div>
      <Navbar />
      <main className="mt-16 border-2 rounded-lg shadow-lg max-w-md mx-auto p-6">
        <h1 className="text-center text-2xl my-3">Data Profile</h1>
        <div className="flex flex-col gap-3">
          <div className="flex items-center justify-between">
            <label htmlFor="">Nama:</label>
            <p>{data.name}</p>
          </div>
          <div className="flex items-center justify-between">
            <label htmlFor="">Username:</label>
            <p>{data.username}</p>
          </div>
          <div className="flex items-center justify-between">
            <label htmlFor="">Email:</label>
            <p>{data.email}</p>
          </div>
        </div>
      </main>
    </div>
  );
}

export default ProfilePage;
