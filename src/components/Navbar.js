import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div className="px-32 py-4 shadow-lg border-b-2 text-blue-500 flex items-center gap-8 text-xl">
      <Link to="/login">
        <p>Login</p>
      </Link>
      <Link to="/register">
        <p>Register</p>
      </Link>
      <Link to="/profile">
        <p>Profile</p>
      </Link>
    </div>
  );
}

export default Navbar;
